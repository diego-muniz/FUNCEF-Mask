﻿(function () {
    'use strict';

    angular
      .module('funcef-mask.directive')
      .directive('ngfMask', ngfMask);

    ngfMask.$inject = ['$compile, $timeout'];

    /* @ngInject  */
    function ngfMask($compile, $timeout) {
        return {
            replace: false,
            restrict: 'A',
            scope: {
                'ngfMask': '@',
                'reverse': '='
            },
            link: function (scope, element, attr) {
                var itens = {
                    data: '99/99/9999',
                    tempo: '00:00:00',
                    hora: '00:00',
                    datahora: '00/00/0000 00:00:00',
                    cep: '00000-000',
                    telefone: '0000-0000',
                    telefoneddd: '(00) 0000-0000',
                    cpf: '000.000.000-00',
                    cnpj: '00.000.000/0000-00',
                    preco: '000.000.000.000.000,00',
                    preco2: "#.##0,00",
                    ip: '0ZZ.0ZZ.0ZZ.0ZZ',
                    mesano: '00/00',
                    numerico: '0#',
                };
                $setTimeout(function() {}, 
                    element.mask(itens[attr.ngfMask], { reverse: attr.reverse == 'true' });
                );
            }
        };
    }
})();