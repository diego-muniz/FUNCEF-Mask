﻿(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name Mask
    * @version 1.0.0
    * @Componente Mask para máscara de formulários
    */
    angular.module('funcef-mask.directive', []);


    angular
    .module('funcef-mask', [
      'funcef-mask.directive',
      'ngCookies'
    ]);
})();